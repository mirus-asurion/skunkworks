This is a test repo.

This repo will be used to test the ability to trigger Jenkins jobs based on push and tag moves.

These are the tags that are present {dev}

To move a tag, call: git tag <tag_name> <commit_id> -f

To push tags, call: git push --tags -f
